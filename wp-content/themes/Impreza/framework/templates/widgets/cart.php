<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * Output WooCommerce shopping cart
 *
 * (!) Important: this file is not intended to be overloaded, so use the below hooks for customizing instead
 *
 * @action Before the template: 'us_before_template:templates/widgets/cart'
 * @action After the template: 'us_after_template:templates/widgets/cart'
 */

if ( ! class_exists( 'woocommerce' ) )
	return;

global $woocommerce;
$link = $woocommerce->cart->get_cart_url();
global $cache_enabled;
?><!-- CART -->

<div class="w-cart">
	<div class="w-cart-h">
		<a class="w-cart-link" href="<?php echo $link; ?>">
			<span class="w-cart-quantity">0</span>
		</a>
		<div class="w-cart-notification">
			<span class="product-name"><?php echo __('Product', 'us'); ?></span>
			<?php echo __('was successfully added to your cart', 'us'); ?>
		</div>
		<div class="w-cart-dropdown">
			<?php the_widget('WC_Widget_Cart'); // This widget being always filled with products via AJAX ?>
		</div>
	</div>
</div><?php
