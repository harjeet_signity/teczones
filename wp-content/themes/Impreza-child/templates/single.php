<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
/**
 * The template for displaying all single posts and attachments
 */
$us_layout = US_Layout::instance();
// Needed for canvas class
//$us_layout->titlebar = 'default';
get_header();

$template_vars = array(
	'metas' => (array) us_get_option( 'post_meta', array() ),
	'show_tags' => in_array( 'tags', us_get_option( 'post_meta', array() ) ),
);

//// Getting header text and image from blog listing page
$image_url		=	get_field( "background_image", 2486 );
$header_text	=	get_field( "header_text", 2486 );	
?>
<!-- MAIN -->
<?php 
	if(!empty($image_url)) {
		?>
			<section class="l-section wpb_row height_medium with_img custom_div_background">
				<div style="background-image: url(<?php echo $image_url; ?>);" class="l-section-img loaded">
				</div>
				<div class="l-section-h i-cf">
					<div class="g-cols offset_small">
						<div class=" full-width"><h1 class="vc_custom_heading hedding-text-bh" style="color: #ffffff;text-align: center"><?php echo $header_text; ?></h1>
						</div>
					</div>
				</div>
			</section>
		<?php
	} 
?>
<div class="l-main">
	<div class="l-main-h i-cf">

		<div class="l-content">
			
			<?php do_action( 'us_before_single' ) ?>

			<?php
			while ( have_posts() ){
				the_post();

				us_load_template( 'templates/blog/single-post', $template_vars );
			}
			?>

			<?php do_action( 'us_after_single' ) ?>

		</div>

		<?php if ( $us_layout->sidebar_pos == 'left' OR $us_layout->sidebar_pos == 'right' ): ?>
			<aside class="l-sidebar at_<?php echo $us_layout->sidebar_pos ?>">
				<?php generated_dynamic_sidebar(); ?>
			</aside>
		<?php endif; ?>

	</div>
</div>
<script>
	jQuery('#menu-item-4802').addClass('active');
</script>
<?php get_footer(); ?>
