<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );
/**
 * The template for displaying pages
 */
$us_layout = US_Layout::instance();
get_header();
us_load_template( 'templates/titlebar' );
?>
<?php 
	/// Fetching image and header text fields for header section
	
	if ( is_home() || is_front_page() ) {	// if is frontpage, fetch nothing	
	} elseif ( is_page() ) {  // if it's normal page, get image and header text
		$image_url		=	get_field( "background_image", get_the_ID() );
		$header_text	=	get_field( "header_text", get_the_ID() );	
	} 
?>
<!-- MAIN -->
<div class="l-main">
	<div class="l-main-h i-cf">

		<div class="l-content">
			<?php 
				if(!empty($image_url)) {
					?>
						<section class="l-section wpb_row height_medium with_img custom_div_background">
							<div style="background-image: url(<?php echo $image_url; ?>);" class="l-section-img loaded">
							</div>
							<div class="l-section-h i-cf">
								<div class="g-cols offset_small">
									<div class=" full-width"><h1 class="vc_custom_heading hedding-text-bh" style="color: #ffffff;text-align: center"><?php echo $header_text; ?></h1>
									</div>
								</div>
							</div>
						</section>
					<?php
				} 
			?>
			<?php do_action( 'us_before_page' ) ?>

			<?php
			while ( have_posts() ) {
				the_post();

				$the_content = apply_filters( 'the_content', get_the_content() );

				// The page may be paginated itself via <!--nextpage--> tags
				$pagination = us_wp_link_pages( array(
					'before' => '<div class="w-blog-pagination"><nav class="navigation pagination" role="navigation">',
					'after' => '</nav></div>',
					'next_or_number' => 'next_and_number',
					'nextpagelink' => '>',
					'previouspagelink' => '<',
					'link_before' => '<span>',
					'link_after' => '</span>',
					'echo' => 0
				) );

				// If content has no sections, we'll create them manually
				$has_own_sections = ( strpos( $the_content, ' class="l-section' ) !== FALSE );
				if ( ! $has_own_sections ) {
					$the_content = '<section class="l-section"><div class="l-section-h i-cf">' . $the_content . $pagination . '</div></section>';
				} elseif ( ! empty( $pagination ) ) {
					$the_content .= '<section class="l-section"><div class="l-section-h i-cf">' . $pagination . '</div></section>';
				}

				echo $the_content;

				// Post comments
				$show_comments = us_get_option( 'page_comments', FALSE );
				if ( $show_comments AND ( comments_open() OR get_comments_number() != '0' ) ) {
					?><section class="l-section for_comments">
						<div class="l-section-h i-cf"><?php
							wp_enqueue_script( 'comment-reply' );
							comments_template();
						?></div>
					</section><?php
				}
			}
			?>

			<?php do_action( 'us_after_page' ) ?>

		</div>

		<?php if ( $us_layout->sidebar_pos == 'left' OR $us_layout->sidebar_pos == 'right' ): ?>
			<aside class="l-sidebar at_<?php echo $us_layout->sidebar_pos ?><?php echo generated_dynamic_sidebar_class() ?>">
				<?php generated_dynamic_sidebar(); ?>
			</aside>
		<?php endif; ?>

	</div>
</div>

<?php get_footer() ?>
