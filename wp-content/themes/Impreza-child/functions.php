<?php
/* Custom functions code goes here. */
global $portfolis;
/////////////// Registering sidebar for phone number starts here /////////////////////////
function phone_register_sidebars() {

	/* Register the primary sidebar. */
	register_sidebar(
		array(
			'name' => 'Phone Number',
			'id' => 'phone-sidebar',			
			'description' => 'You can add contact number in this area.',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h4>',
			'after_title' => '</h4>',
		)
	);

	/* Repeat register_sidebar() code for additional sidebars. */
}
add_action( 'widgets_init', 'phone_register_sidebars' );
/////////////// Registering sidebar for phone number Ends Here /////////////////////////

//--------------------------------------------------------------------------------------------------------------------------------------------------------//

////////////////// Allowing span in Head Tag (starts here) //////////////////////////////
function myextensionTinyMCE($init) {
    // Command separated string of extended elements
    $ext = 'span[id|name|class|style],a[href|target]';

    // Add to extended_valid_elements if it alreay exists
    if ( isset( $init['extended_valid_elements'] ) ) {
        $init['extended_valid_elements'] .= ',' . $ext;
    } else {
        $init['extended_valid_elements'] = $ext;
    }

    // Super important: return $init!
    return $init;
}

add_filter('tiny_mce_before_init', 'myextensionTinyMCE' );
////////////////// Allowing span in Head Tag (Ends here) ////////////////////////////////

//--------------------------------------------------------------------------------------------------------------------------------------------------------//

////////////////// Adding responsive stylesheet (starts here) ////////////////////////////
function wptuts_scripts_basic()
{
    // For either a plugin or a theme, you can then enqueue the script:
    wp_enqueue_style( 'responsive', get_stylesheet_directory_uri() . '/responsive.css');    
}
add_action( 'wp_enqueue_scripts', 'wptuts_scripts_basic' );
////////////////// Adding responsive stylesheet (ends here) //////////////////////////////

////////////////// website-portfolio shortcode (starts here) //////////////////////////////

function get_website_portfolio( $atts ){
	$returnHtml		=	"<div class='g-cols wpb_row offset_small'>";	
	$portfoliios 	= get_field('portfoliio');
	if( isset( $portfoliios[0]['single_portfolio'] ) && !empty( $portfoliios[0]['single_portfolio'] ) ){
		foreach( $portfoliios[0]['single_portfolio'] as $key=>$single_portfolio ) {
			if( 0 == $key ){
				$returnHtml	.=	"<div class='one-half'>
									<div class='portfolio-left' style='background-image:url({$single_portfolio['portfolio_image']});'>
                                        <div class='portfolio-left-innr'>";
								
				if( !empty( $single_portfolio['logo'] ) ){
					$returnHtml	.=		"<img src='{$single_portfolio['logo']}'>";
				}
				if( !empty( $single_portfolio['short_description'] ) ){
					$returnHtml	.=		"<p>{$single_portfolio['short_description']}</p>";
				}
				if( !empty( $single_portfolio['portfolio_link'] ) ){
					$returnHtml	.=		"<a class='btn btn-primary' target='_blank' href='{$single_portfolio['portfolio_link']}'>Read More</a>";
				} 
				
				$returnHtml	.=	"	   </div>
                                    </div>
								</div>";
			}
			else {
				if( 1 == $key )
					$returnHtml	.=	"<div class='one-half right-sps'><div class='portfolio-right'>";
										
				if( 1 == $key || 3 == $key)
					$returnHtml	.=	"<div class='g-cols wpb_row offset_small'>";
				
				
					$returnHtml	.=	"<div class='one-half'>
										<div class='portfolio-right-child' style='background-image:url({$single_portfolio['portfolio_image']});'>
                                            <div class='portfolio-right-child-innr'>";
										
					if( !empty( $single_portfolio['logo'] ) ){
						$returnHtml	.=		"<img src='{$single_portfolio['logo']}'>";
					}
					if( !empty( $single_portfolio['portfolio_link'] ) ){
						$returnHtml	.=		"<a class='btn btn-primary' target='_blank' href='{$single_portfolio['portfolio_link']}'>Read More</a>";
					}		
													
					$returnHtml	.=	"	  </div>
                                        </div>
									</div>";
				
				if(2 == $key )
					$returnHtml	.=	"</div>";
				
				if( 4 == $key )	
					$returnHtml	.=	"</div></div>";
								
			}
		}
	} 
	else {
		$returnHtml	.=	"<div class='full'><h3 style='align:center'>No Portfolios found !</h3></div>";
	}
	//if( isset( $_GET['test'] ) ) pre($portfoliios);
	
	$returnHtml		.=	"</div>";
	
	if( !( isset( $_GET['test'] ) ) ){
		//$returnHtml	=	"<div class='g-cols wpb_row offset_small'><img src='http://teczones.com/wp-content/uploads/2016/03/blog.png' style='width:100%'></div>";
	}
	return $returnHtml;
}
add_shortcode('website_portfolio', 'get_website_portfolio');

function get_apps_portfolio( $atts ){
	
	$returnHtml		=	"<div class='g-cols wpb_row offset_small'>";	
	$portfoliios 	= get_field('portfoliio');
	if( isset( $portfoliios[1]['single_portfolio'] ) && !empty( $portfoliios[1]['single_portfolio'] ) ){
		foreach( $portfoliios[1]['single_portfolio'] as $key=>$single_portfolio ) {
			if( 4 == $key ){
				$returnHtml	.=	"<div class='one-half right-sps'>
									<div class='portfolio-left' style='background-image:url({$single_portfolio['portfolio_image']});'>
                                        <div class='portfolio-left-innr'>";
								
				if( !empty( $single_portfolio['logo'] ) ){
					$returnHtml	.=		"<img src='{$single_portfolio['logo']}'>";
				}
				if( !empty( $single_portfolio['short_description'] ) ){
					$returnHtml	.=		"<p>{$single_portfolio['short_description']}</p>";
				}
				if( !empty( $single_portfolio['portfolio_link'] ) ){
					$returnHtml	.=		"<a class='btn btn-primary' target='_blank' href='{$single_portfolio['portfolio_link']}'>Read More</a>";
				} 
				
				$returnHtml	.=	"	   </div>
                                    </div>
								</div>";
			}
			else {
				if( 0 == $key )
					$returnHtml	.=	"<div class='one-half'><div class='portfolio-right'>";
										
				if( 0 == $key || 2 == $key)
					$returnHtml	.=	"<div class='g-cols wpb_row offset_small'>";
				
				
					$returnHtml	.=	"<div class='one-half'>
										<div class='portfolio-right-child' style='background-image:url({$single_portfolio['portfolio_image']});'>
                                            <div class='portfolio-right-child-innr'>";
										
					if( !empty( $single_portfolio['logo'] ) ){
						$returnHtml	.=		"<img src='{$single_portfolio['logo']}'>";
					}
					if( !empty( $single_portfolio['portfolio_link'] ) ){
						$returnHtml	.=		"<a class='btn btn-primary' target='_blank' href='{$single_portfolio['portfolio_link']}'>Read More</a>";
					}		
													
					$returnHtml	.=	"	  </div>
                                        </div>
									</div>";
				
				if(1 == $key )
					$returnHtml	.=	"</div>";
						
				if( 3 == $key )	
					$returnHtml	.=	"</div></div></div>";
			}
			
		}
	} 
	else {
		$returnHtml	.=	"<div class='full'><h3 style='align:center'>No Portfolios found !</h3></div>";
	}
		
	$returnHtml		.=	"</div>";
		
	return $returnHtml;
}
add_shortcode('apps_portfolio', 'get_apps_portfolio');

////////////////// website-portfolio shortcode (ends here) ////////////////////////////////

function pre( $objOrArray, $die=true ){
	echo '<pre>';print_r($objOrArray);
	if($die) die;
}
